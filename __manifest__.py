# -*- coding: utf-8 -*-
{
    'name': "VA Project",
    'version': "1.0",
    'summary': "VA Project",

    'author': "VoltusAutomation, KRKA",
    'website': "http://www.voltusautomation.com/va_project",
    'category': "Enterprise Component Management",

    # any module necessary for this one to work correctly
    'depends': ['base', 'project', 'mail', 'sales_team', 'hr_timesheet'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/project_views.xml',
        'views/supplier_po_views.xml',
        'views/change_notices_views.xml',
        'views/invoice_views.xml',
        'views/po_details_views.xml',
        'views/drawings_views.xml',
        'xml/va_ppm_template.xml',
        'xml/assets.xml',
    ],
}