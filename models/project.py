# -*- coding: utf-8 -*-

from odoo import models, fields, api
from . import utils
import plotly
import plotly.graph_objs

class VAProject(models.Model):
    _name = "project.project"
    _inherit = ['project.project']


    # Original Values
    @api.depends('originalLabourCost', 'originalMaterialCost')
    @api.one
    def _originalCost(self):
        self.originalCost = self.originalLabourCost + self.originalMaterialCost

    @api.depends('originalSellPrice', 'originalCost')
    @api.one
    def _originalMargin(self):
        self.originalMargin = utils.margin(self.originalSellPrice, self.originalCost)

    @api.depends('originalSellPrice', 'originalCost')
    @api.one
    def _originalProfit(self):
        self.originalProfit = self.originalSellPrice - self.originalCost


    # Initial values
    @api.depends('initialLabourCost', 'initialMaterialCost')
    @api.one
    def _initialCost(self):
        self.initialCost = self.initialLabourCost + self.initialMaterialCost


    @api.depends('initialSellPrice', 'initialCost')
    @api.one
    def _initialMargin(self):
        self.initialMargin = utils.margin(self.initialSellPrice, self.initialCost)

    @api.depends('initialSellPrice', 'initialCost')
    @api.one
    def _initialProfit(self):
        self.initialProfit = self.initialSellPrice - self.initialCost

    # Base calculated Values are the sum of all Initial values on PO Model
    @api.depends('supplier_po_id')
    @api.multi
    def _baseSellPrice(self):
        for po in self:
            self.baseSellPrice = sum(po_id.baseSellPrice for po_id in po.supplier_po_id)

    @api.depends('supplier_po_id')
    @api.multi
    def _baseCost(self):
        for po in self:
            self.baseCost = sum(po_id.baseCost for po_id in po.supplier_po_id)

    @api.depends('baseSellPrice', 'baseCost')
    def _baseMargin(self):
        self.baseMargin = utils.margin(self.baseSellPrice, self.baseCost)

    @api.depends('baseSellPrice', 'baseCost')
    @api.one
    def _baseProfit(self):
        self.baseProfit = self.baseSellPrice - self.baseCost


    #toDate
    @api.depends('supplier_po_id')
    @api.multi
    def _toDateBilled(self):
        for po in self:
            self.toDateBilled = sum(po_id.billedToDate for po_id in po.supplier_po_id)

    @api.depends('task_ids')
    @api.multi
    def _toDateLabourCost(self):
        for task in self:
            self.toDateLabourCost = sum(task_id.total_cost_spent for task_id in task.task_ids)

    @api.depends('supplier_po_id')
    @api.multi
    def _toDateMaterialCost(self):
        for po in self:
            self.toDateMaterialCost = sum(po_id.costToDate for po_id in po.supplier_po_id)

    @api.depends('toDateLabourCost', 'toDateMaterialCost')
    @api.one
    def _toDateTotalCost(self):
        self.toDateTotalCost = self.toDateLabourCost + self.toDateMaterialCost

    @api.depends('toDateLabourCost', 'toDateMaterialCost')
    @api.one
    def _toDateProfit(self):
        self.toDateTotalCost = self.toDateLabourCost + self.toDateMaterialCost


    @api.depends('toDateBilled', 'toDateTotalCost')
    @api.multi
    def _toDateMargin(self):
        self.toDateMargin = utils.margin(self.toDateBilled, self.toDateTotalCost)

    @api.depends('toDateBilled', 'toDateTotalCost')
    @api.one
    def _toDateProfit(self):
        self.toDateProfit = self.toDateBilled - self.toDateTotalCost



    #Remaining values are the sum of all current values on PO Model
    @api.depends('supplier_po_id')
    @api.multi
    def _remainingToBeBilled(self):
        for po in self:
            self.remainingToBeBilled = sum(po_id.remainingToBeBilled for po_id in po.supplier_po_id)

    @api.depends('currentLabourCost', 'toDateLabourCost')
    @api.one
    def _remainingLabourCost(self):
        for po in self:
            self.remainingLabourCost = self.currentLabourCost - self.toDateLabourCost

    @api.depends('currentMaterialCost', 'toDateMaterialCost')
    @api.one
    def _remainingMaterialCost(self):
        for po in self:
            self.remainingMaterialCost = self.currentMaterialCost - self.toDateMaterialCost

    @api.depends('currentTotalCost', 'toDateTotalCost')
    @api.one
    def _remainingTotalCost(self):
        for po in self:
            self.remainingTotalCost = self.currentTotalCost - self.toDateTotalCost


    @api.depends('remainingToBeBilled', 'remainingTotalCost')
    @api.one
    def _remainingProfit(self):
        self.remainingProfit = self.remainingToBeBilled - self.remainingTotalCost



    @api.depends('remainingToBeBilled', 'remainingTotalCost')
    @api.multi
    def _remainingMargin(self):
        self.remainingMargin = utils.margin(self.remainingToBeBilled, self.remainingTotalCost)


    # Change Values
    @api.depends('initialSellPrice', 'currentSellPrice')
    @api.one
    def _changeSell(self):
        self.changesSell = self.currentSellPrice - self.initialSellPrice

    @api.depends('currentTotalCost', 'initialCost')
    @api.one
    def _changeCost(self):
        self.changesCost = self.currentTotalCost - self.initialCost

    @api.depends('changesSell', 'changesCost')
    @api.one
    def _changeMargin(self):
        self.changeMargin = utils.margin(self.changesSell, self.changesCost)


    # Current values
    @api.depends('supplier_po_id')
    @api.multi
    def _currentSellPrice(self):
        for po in self:
            self.currentSellPrice = sum(po_id.currentSellPrice for po_id in po.supplier_po_id)


    # TODO: verify depends
    @api.depends('task_ids', 'task_ids.total_cost_spent')
    @api.multi
    def _currentLabourCost(self):
        for task in self:
            self.currentLabourCost = sum(task_id.total_cost_spent for task_id in task.task_ids)


    # Current cost is the sum of all PO current costs. (Current material cost)
    @api.depends('supplier_po_id')
    @api.multi
    def _currentMaterialCost(self):
        for po in self:
            self.currentMaterialCost = sum(po_id.currentCost for po_id in po.supplier_po_id)

    @api.depends('currentLabourCost', 'currentMaterialCost')
    @api.one
    def _currentTotalCost(self):
        self.currentTotalCost = self.currentLabourCost + self.currentMaterialCost



    @api.depends('currentSellPrice', 'currentTotalCost')
    @api.one
    def _currentMargin(self):
        self.currentMargin = utils.margin(self.currentSellPrice, self.currentTotalCost)


    @api.depends('currentSellPrice', 'toDateBilled')
    @api.one
    def _toBeBilled(self):
        self.toBeBilled = self.currentSellPrice - self.toDateBilled


    @api.depends('currentSellPrice', 'currentTotalCost')
    @api.one
    def _currentProfit(self):
        self.currentProfit = self.currentSellPrice - self.currentTotalCost

    # Current values
    @api.depends('task_ids')
    @api.multi
    def _currentHours(self):
        for task in self:
            self.currentHours = sum(task_id.total_hours_spent for task_id in task.task_ids)


    @api.depends('currentHours', 'baseHours')
    @api.one
    def _percentageHours(self):
        if self.baseHours > 0:
            self.percentageHours = round(100.0 * self.currentHours / self.baseHours, 2)


    @api.depends('supplier_cytc_id')
    @api.multi
    def _costYetToCome(self):
        for cytc in self:
           self.costYetToCome = sum(cytc_id.currentCost for cytc_id in cytc.supplier_cytc_id)

    @api.depends('currentTotalCost', 'toDateTotalCost')
    @api.one
    def _committedCost(self):
        self.committedCost = self.currentTotalCost - self.toDateTotalCost

    @api.depends('toDateTotalCost', 'committedCost', 'costYetToCome')
    @api.one
    def _totalProjectCost(self):
        self.totalProjectCost = self.toDateTotalCost + self.committedCost + self.costYetToCome


    #Variance
    @api.depends('initialProfit', 'currentProfit')
    @api.one
    def _varianceProfit(self):
        self.varianceProfit = self.currentProfit - self.initialProfit

    @api.depends('currentProfit', 'initialProfit')
    @api.one
    def _varianceProfitPercentage(self):
        if self.initialProfit != 0:
            self.varianceProfitPercentage = round(100.0 * (1 - self.currentProfit / self.initialProfit), 2)

    @api.depends('initialLabourCost', 'currentLabourCost')
    @api.one
    def _varianceLabourCost(self):
        self.varianceLabourCost = self.currentLabourCost - self.initialLabourCost

    @api.depends('initialLabourCost', 'currentLabourCost')
    @api.one
    def _varianceLabourCostPercentage(self):
        if self.initialLabourCost != 0:
         self.varianceLabourCostPercentage = round(100.0 * (1 - self.currentLabourCost / self.initialLabourCost), 2)


    @api.depends('initialMaterialCost', 'currentMaterialCost')
    @api.one
    def _varianceMaterialCost(self):
        self.varianceMaterialCost = self.currentMaterialCost - self.initialMaterialCost

    @api.depends('initialMaterialCost', 'currentMaterialCost')
    @api.one
    def _varianceMaterialCostPercentage(self):
        if self.initialMaterialCost != 0:
         self.varianceMaterialCostPercentage = round(100.0 * (1 - self.currentMaterialCost / self.initialMaterialCost), 2)

    #Total Cost
    @api.depends('varianceMaterialCost', 'varianceLabourCost')
    @api.one
    def _varianceTotalCost(self):
        self.varianceTotalCost = self.varianceMaterialCost + self.varianceLabourCost

    @api.depends('initialMaterialCost', 'currentTotalCost')
    @api.one
    def _varianceTotalCostPercentage(self):
        if (self.initialMaterialCost + self.initialLabourCost) != 0:
            self.varianceTotalCostPercentage = round(100.0 * (1 - self.currentTotalCost / self.initialCost), 2)


    #Variance sale price
    @api.depends('initialSellPrice', 'currentSellPrice')
    @api.one
    def _varianceSell(self):
        self.varianceSell = self.currentSellPrice - self.initialSellPrice

    @api.depends('initialSellPrice', 'currentSellPrice')
    @api.one
    def _varianceSellPercentage(self):
        if self.currentSellPrice != 0:
            self.varianceSellPercentage = round(100.0 * (1 - self.initialSellPrice / self.currentSellPrice), 2)


    #Variance current Margin
    @api.depends('initialMargin', 'currentMargin')
    @api.one
    def _varianceMargin(self):
        self.varianceMargin = self.currentMargin - self.initialMargin



    @api.depends('initialSellPrice', 'initialCost', 'initialSellPrice', 'initialCost', 'initialProfit', 'initialMargin',
                 'currentSellPrice', 'currentLabourCost', 'currentMaterialCost', 'currentProfit', 'currentMargin',)
    @api.one
    def _barChart(self):
        for rec in self:
            labels =        ['Sell Price',          'Lab. Cost',            'Mat. Cost',              'Total Cost',                                     'Profit',            'Margin' ]
            valuesInitial = [self.initialSellPrice, self.initialLabourCost, self.initialMaterialCost, self.initialLabourCost + self.initialCost,         self.initialProfit, self.initialMargin]
            valuesCurrent = [self.currentSellPrice, self.currentLabourCost, self.currentMaterialCost, self.currentLabourCost + self.currentMaterialCost, self.currentProfit, self.currentMargin]
            rec.barChart = plotly.offline.plot({
                "data": [
                    plotly.graph_objs.Bar(x=labels, y=valuesInitial, name="Initial"),
                    plotly.graph_objs.Bar(x=labels, y=valuesCurrent, name="Current"),

                ]
            }, include_plotlyjs=False, output_type='div' )

    @api.depends('initialSellPrice', 'initialCost', 'currentSellPrice', 'currentMaterialCost')
    @api.one
    def _pieChart(self):
        for rec in self:
            labels = ['Profit', 'Mat. Cost', 'Lab. cost']
            values = [self.currentProfit, self.currentMaterialCost, self.currentLabourCost,]
            rec.pieChart = plotly.offline.plot({
                "data": [
                    plotly.graph_objs.Pie(labels=labels, values=values),
                ]
            }, include_plotlyjs=False, output_type='div' )

    barChart = fields.Text(string='Bar Chart', compute='_barChart')

    pieChart = fields.Text(string='Pie Chart', compute='_pieChart')


    site_contact_id = fields.Many2one('res.partner', string='Customer', auto_join=True, track_visibility='onchange')

    supplier_po_id = fields.One2many('project.supplier_po', 'project_id', 'PO #', help='The list of Supplier PO', domain=[('isPO', '=', True)])
    supplier_cytc_id = fields.One2many('project.supplier_po', 'project_id', 'PO #', help='The list of Supplier PO', domain=[('isPO', '=', False)])

    drawings_id = fields.One2many('project.drawings', 'project_id') #string='Change Notices', auto_join=True, track_visibility='onchange')

    customerPo = fields.Char(string='Customer PO', required = True)
    siteAddress = fields.Char(string='Site Address', required = True)

    # Original Estimated amount
    originalSellPrice = fields.Monetary(string='Original Sell Price', currency_field='currency_id', required=False, help="Original estimated price")
    originalLabourCost = fields.Monetary(string='Original Labour Cost', currency_field='currency_id', required=False, help="Original labour cost ")
    originalMaterialCost = fields.Monetary(string='Original Material Cost', currency_field='currency_id', required=False, help="Original material cost ")
    originalCost = fields.Monetary(compute="_originalCost", string='Original Total Cost', currency_field='currency_id', required=False, help="Original estimated cost ", readonly=True)
    originalProfit = fields.Monetary(compute="_originalProfit", string='Original Profit', currency_field='currency_id', required=False, help="Original profit")
    originalMargin = fields.Float(compute="_originalMargin", string='Original Margin', help="Orifinal estimated margin")


    # Initial values are the Negotiation Values
    initialSellPrice = fields.Monetary(string='Initial Price', currency_field='currency_id', required=False, help="Inital price during negotiation")
    initialLabourCost = fields.Monetary(string='Initial Labour Cost', currency_field='currency_id', required=False, help="Initial labour cost ")
    initialMaterialCost = fields.Monetary(string='Initial Material Cost', currency_field='currency_id', required=False, help="Initial material cost ")
    initialCost = fields.Monetary(compute="_initialCost", string='Initial Cost', currency_field='currency_id', required=False, help="Inital cost during negotiation", readonly=True)
    initialProfit = fields.Monetary(compute="_initialProfit", string='Initial Profit', currency_field='currency_id', required=False, help="Initial profit")
    initialMargin = fields.Float(compute="_initialMargin", string='Initial Margin', help="Inital margin during negotiation")


    # Base is the sum of all PO Base price
    baseSellPrice = fields.Monetary(compute='_baseSellPrice', string='Base Sell Price', currency_field='currency_id', required=False, help="Sum of all PO's base sell price")
    baseCost = fields.Monetary(compute='_baseCost',string='Base Cost', currency_field='currency_id', required=False, help="Sum of all PO's base cost")
    baseProfit = fields.Monetary(compute='_baseProfit',string='Base Profit', currency_field='currency_id', required=False, help="Sum of all PO's base profit")
    baseMargin = fields.Float(compute="_baseMargin", string='Base Margin', help="BaseSellPrice / BaseCostPrice")

    # To date values are the sum of all ToDate values on POs
    toDateBilled = fields.Monetary(compute="_toDateBilled", string='To Date Sell Price', currency_field='currency_id', readonly=1, help="Sum of all Invoice's Sell Price")
    toDateLabourCost = fields.Monetary(compute="_toDateLabourCost", string='To Date Labour Cost', currency_field='currency_id', readonly=1, help="Sum of all TimeSheets Cost")
    toDateMaterialCost = fields.Monetary(compute="_toDateMaterialCost", string='To Date Material Cost', currency_field='currency_id', readonly=1, help="Sum of all Invoice's Cost")
    toDateTotalCost = fields.Monetary(compute="_toDateTotalCost", string='To Date Total Cost', currency_field='currency_id', readonly=1, help="To Date Labour Cost + To Date Material Cost")
    toDateProfit = fields.Monetary(compute='_toDateProfit',string='To Date Profit', currency_field='currency_id', required=False, help="Sum of all PO's to date profit")
    toDateMargin = fields.Float(compute="_toDateMargin", string='To Date Margin', readonly=1, help="ToDateTotalCost / ToDateBilled")

    #Remaining is de sum of all Remaining fields on POs
    remainingToBeBilled = fields.Monetary(compute="_remainingToBeBilled", string='Remaining Sell Price', currency_field='currency_id', readonly=1, help="remainingToBeBilled = CurrentSellPrice - ToDateBilled. currentSellPrice = baseSellPrice-changeNoticesPrice")
    remainingLabourCost = fields.Monetary(compute="_remainingLabourCost", string='Remaining Lab. Cost', currency_field='currency_id', readonly=1, help="remainingLabourCost = currentLabourCost - toDateLabourCost. currentCost = baseCost-changeNoticesCost")
    remainingMaterialCost = fields.Monetary(compute="_remainingMaterialCost", string='Remaining Mat. Cost', currency_field='currency_id', readonly=1, help="remainingMaterialCost = currentMaterialCost - toDateMaterialCost. currentMaterialCost = baseCost-changeNoticesCost")
    remainingTotalCost = fields.Monetary(compute="_remainingTotalCost", string='Remaining Total Cost', currency_field='currency_id', readonly=1, help="remainingTotalCost = currentTotalCost - toDateTotalCost. currentTotalCost = baseCost-changeNoticesCost")
    remainingProfit = fields.Monetary(compute='_remainingProfit',string='Remaining Profit', currency_field='currency_id', required=False, help="Sum of all PO's remaining profit")
    remainingMargin = fields.Float(compute="_remainingMargin", string='Remaining Margin', readonly=1, help="remainingTotalCost / remainingToBeBilled")

    # Billed
    toBeBilled = fields.Monetary(compute="_toBeBilled",string='To be Billed', currency_field='currency_id', help="Current Sell price - TodateBilled")

    #Change Values
    changesSell = fields.Monetary(compute="_changeSell",string='Changes Sell', currency_field='currency_id', help="NegoSellPrice - CurrentSellPrice")
    changesCost = fields.Monetary(compute="_changeCost", string='Changes Cost', currency_field='currency_id', help="Initial Cost - Current Cost")
    changeMargin = fields.Float(compute="_changeMargin", string='Changes Margin', readonly=1, help="changeNoticeCost / changeNoticeSell")

    #Current fields depends on PO
    currentSellPrice = fields.Monetary(compute="_currentSellPrice", string='Current Sell Price', currency_field='currency_id', readonly=1, help="Sum of all PO's (BaseSellPrice-ChangeNoticesSell)")
    currentLabourCost = fields.Monetary(compute="_currentLabourCost", string="Current Labour Cost", help="Cost asociated to timesheets up to date")
    currentMaterialCost = fields.Monetary(compute="_currentMaterialCost", string='Current Mat. Cost', currency_field='currency_id', readonly=1, help="Sum of all PO's (BaseCost-ChangeNoticesCost)")
    currentTotalCost = fields.Monetary(compute="_currentTotalCost", string='Current Total Cost', currency_field='currency_id', readonly=1, help="Sum of all PO's (BaseCost-ChangeNoticesCost)")
    currentProfit = fields.Monetary(compute='_currentProfit',string='Current Profit', currency_field='currency_id', required=False, help="Sum of all PO's current profit")
    currentMargin = fields.Float(compute="_currentMargin", string='Current Margin', readonly=1, help="currentCost / currentSellPrice")

    #Total hours per project
    baseHours = fields.Float(string="Planned Hours", help="Planed Labour (Hours)")
    currentHours = fields.Float(compute="_currentHours", string="Spent Hours", help="Sum of all timesheets up to date")
    percentageHours = fields.Float(compute="_percentageHours", string="Spent Hours(%)", help=" currentHours / basHours")

    #Costs
    costYetToCome = fields.Monetary(compute="_costYetToCome", string="Cost Yet To Come", currency_field='currency_id', readonly=1, help="Is the sum of all cost that are not a PO yet")
    committedCost = fields.Monetary(compute="_committedCost", string="Committed Cost", currency_field='currency_id', readonly=1, help="Is the sum of all cost that are on PO, but the invoice is not received yet")
    totalProjectCost = fields.Monetary(compute="_totalProjectCost", string="Total Project Cost", currency_field='currency_id', readonly=1, help="Actual Cost + committed cost + CYTC")

    #Variance
    varianceProfit = fields.Monetary(compute="_varianceProfit", string="Variance Profit", currency_field='currency_id', readonly=1, help="Profit Variance = Estimated Profit - Real Profit")
    varianceProfitPercentage = fields.Float(compute="_varianceProfitPercentage", string="Variance Profit %", readonly=1, help="Profit Variance (%) = Real Profit / Estimated Profit")

    varianceLabourCost = fields.Monetary(compute="_varianceLabourCost", string="Variance Labour Cost", currency_field='currency_id', readonly=1, help="Labour Cost Variance = Estimated Labor Cost - Real Labor Cost")
    varianceLabourCostPercentage = fields.Float(compute="_varianceLabourCostPercentage", string="Variance Labor Cost %", help="Labor Cost Variance (%) = Real Labor Cost / Estimated Labor Cost")

    varianceMaterialCost = fields.Monetary(compute="_varianceMaterialCost", string="Variance Mat. Cost", currency_field='currency_id', readonly=1, help="Material Cost Variance = Estimated Material Cost - Real Material Cost")
    varianceMaterialCostPercentage = fields.Float(compute="_varianceMaterialCostPercentage", string="Variance Mat. Cost %", readonly=1, help="Material Cost Variance (%) = Real Material Cost / Estimated Material Cost")

    varianceSell = fields.Monetary(compute="_varianceSell", string="Variance Sell Price", currency_field='currency_id', readonly=1, help="Sell Price Variance = Estimated Sell Price - Real Sell Price")
    varianceSellPercentage = fields.Float(compute="_varianceSellPercentage", string="Variance Sell Price %", readonly=1, help="Sell Price Variance (%) = Real Sell Price / Estimated Sell Price")

    varianceTotalCost = fields.Monetary(compute="_varianceTotalCost", string="Variance Total Cost", currency_field='currency_id', readonly=1, help="Total Cost Variance = Estimated Total Cost - Real Total Cost")
    varianceTotalCostPercentage = fields.Float(compute="_varianceTotalCostPercentage", string="Variance Total Cost %", readonly=1, help="Total Cost Variance (%) = Real Total Cost / Estimated Total Cost")

    varianceMargin = fields.Float(compute="_varianceMargin", string="Variance Margin", readonly=1, help="Variance Current Margin = Curent Margin(%) - Initial Margin(%)")

    projectGraph = fields.Text(string='Project Graph', compute='_projectGraph', )

    @api.multi
    def show_va_project(self):
        self.ensure_one()
        view_id = self.env.ref('va_ppm.project_form').id
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'project.project',  # this model
            'res_id': self.id,  # the current wizard record
            'view_mode': 'form',
            'view_id': view_id,
            'context': {'form_view_initial_mode': 'edit'},
        }

class Task(models.Model):
    _inherit = "project.task"

    @api.depends('timesheet_ids.cost_amount')
    @api.multi
    def _total_cost_spent(self):
        for task in self:
            task.total_cost_spent = round(sum(task.timesheet_ids.mapped('cost_amount')), 2)


    total_cost_spent = fields.Float("Current Labour Cost", compute="_total_cost_spent", store=True, help="Computed as: Current Labor Cost.")

class AccountAnalyticLine(models.Model):
    _inherit = 'account.analytic.line'

    cost_amount = fields.Monetary(compute="_cost_amount", string='Cost Amount')

    @api.one
    @api.depends('unit_amount', 'employee_id.timesheet_cost')
    def _cost_amount(self):
        self.cost_amount = self.unit_amount * self.employee_id.timesheet_cost


#TODO: Add change notices column to PO views
#TODO: Invoice should validate the amount of total PO and show the PO Numbers