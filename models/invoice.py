# -*- coding: utf-8 -*-

from odoo import models, fields, api
from . import utils

class VAInvoice(models.Model):
    _name = "project.invoice"
    # _inherit = ['project.project']
    _inherit = ['mail.thread']

    @api.depends('billingSell','billingCost')
    @api.one
    def _invoiceMargin(self):
        self.invoiceMargin = utils.margin(self.billingSell, self.billingCost)

    @api.depends('supplier_po_id.currentCost', 'totalBillingCostPercent')
    @api.one
    def _billingCost(self):
        self.billingCost = self.supplier_po_id.currentCost * self.totalBillingCostPercent / 100.0

    @api.depends('supplier_po_id.currentSellPrice', 'totalBillingInvoicePercent')
    @api.one
    def _billingSell(self):
        self.billingSell = self.supplier_po_id.currentSellPrice * self.totalBillingInvoicePercent / 100.0

    @api.depends('supplier_po_id.currentCost', 'billingCost')
    @api.one
    def _poCostRemaining(self):
        self.poCostRemaining = self.billingCost - self.supplier_po_id.currentCost

    @api.depends('supplier_po_id.currentSellPrice', 'billingSell')
    @api.one
    def _poSellRemaining(self):
        self.poSellRemaining = self.billingSell - self.supplier_po_id.currentSellPrice

    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    currency_id = fields.Many2one('res.currency', related="company_id.currency_id", string="Currency", readonly=True)

    supplier_po_id = fields.Many2one('project.supplier_po', string='Supplier PO', readonly=True)
    gpInvoice = fields.Char(string = 'GP Invoice #', required = True)
    supplierInvoice = fields.Char(string = 'MFG Inv #', required = False)
    supplierInvoiceDate = fields.Date(string = 'MFG Billing Date')
    entryDate = fields.Date(string = 'Entry Date')

    totalBillingCostPercent = fields.Float(string='Total Billing Cost(%)')
    totalBillingInvoicePercent = fields.Float(string='Total Billing Invoice (%)')

    billingCost = fields.Monetary(compute="_billingCost", string='Billing Cost', currency_field='currency_id', required=True) #po.currentCost*totalBillingCostPercent
    billingSell = fields.Monetary(compute="_billingSell", string='Billing Sell', currency_field='currency_id', required=True) #po.currentSell*totalBillingInvoicePercent
    invoiceMargin = fields.Float(compute="_invoiceMargin", string="Invoice Margin")#fields.Float() #utils.margin(billingSell, billingCost)

    poCostRemaining = fields.Monetary(compute="_poCostRemaining", string='PO Cost Remaining', currency_field='currency_id', required=True) # billingCost - po.currentCost
    poSellRemaining = fields.Monetary(compute="_poSellRemaining", string='PO Sell Remaining', currency_field='currency_id', required=True) # biliingSell - po.currentSellPrice

    @api.multi
    def show_va_invoice(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'project.invoice',  # this model
            'res_id': self.id,  # the current wizard record
            'view_mode': 'form',
            'view_type': 'form',
            'views': [[False, 'form']],
            'context': {'form_view_initial_mode': 'edit'},
        }

    
