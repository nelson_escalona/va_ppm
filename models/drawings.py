from odoo import models, fields, api
from datetime import date
#from odoo.addons import decimal_precision as dp

class VADrawings(models.Model):
    _name = "project.drawings"
    # _description = 'Purchase Order Line'
    # _inherit = ['project.project']
    _inherit = ['mail.thread']
    # TODO: inherit from Documents?

    @api.one
    def _project_id(self):
        self.project_id = self.env.context.get('default_project_id')

    readonly_po = fields.Boolean(string='Read Only')

    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)

    # project_id = fields.Many2one('project.project', string='Project',
    #                              default=lambda self: self.env.context.get('default_project_id'), auto_join=True, track_visibility='onchange')

    project_id = fields.Many2one('project.project', string='Project', auto_join=True, track_visibility='onchange', store=True)

    supplier_po_id = fields.Many2one('project.supplier_po', string='Supplier PO', auto_join=True, track_visibility='onchange')

    po_details_id = fields.Many2one('project.po_details', string='Item', auto_join=True, track_visibility='onchange')

    description = fields.Text(string='Description')

    dateDwgRequestTS = fields.Date(string='DWG Request TS Date')
    dateDwgSubmittalTC = fields.Date(string='DWG Submital TC Date')
    dateDwgReturnedFC = fields.Date(string='DWG Returned FC Date')
    dateDwgStatusRejected = fields.Date(string='DWG Status Rejected Date')
    dateDwgStatusAcceptable = fields.Date(string='DWG Status Acceptable Date')
    dateDwgStatusApproved = fields.Date(string='DWG Status Approved Date')
    dateDwgReturnedTS = fields.Date(string='DWG Returned TS Date')
    dateDwgFollowUpTS = fields.Date(string='DWG Follow Up TS Date')
    dateDwgSubmittedTC = fields.Date(string='R-DWG Submitted TC Date')
    dateDwgStatusTC = fields.Date(string='R-DWG Status TC Date')
    dateDwgApprovedReceived = fields.Date(string='R-DWG Approved Received Date')
    dateDwgSubmittalTS = fields.Date(string='A-DWG Submittal TS Date')

    @api.multi
    def show_drawings(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'project.drawings',  # this model
            'res_id': self.id,  # the current wizard record
            'view_mode': 'form',
            'view_type': 'form',
            'views': [[False, 'form']],
            'context': {'form_view_initial_mode': 'edit'},
        }