# -*- coding: utf-8 -*-

from odoo import models, fields, api
#from utils import VAutils

class VAChangeNotices(models.Model):
    _name = "project.change_notices"
    # _inherit = ['project.project']
    _inherit = ['mail.thread']

    @api.depends('deleteAmount', 'deleteMargin')
    @api.one
    def _deleteChangeSell(self):
        self.deleteChangeSell = self.deleteAmount * (1 - self.deleteMargin / 100.0)

    @api.depends('addAmount', 'addMargin')
    @api.one
    def _addChangeSell(self):
        self.addChangeSell = self.addAmount / (1 - self.addMargin / 100.0)

    @api.depends('addAmount', 'deleteAmount')
    @api.one
    def _totalChangeCost(self):
        self.totalChangeCost = self.addAmount - self.deleteAmount

    @api.depends('addChangeSell', 'deleteChangeSell')
    @api.one
    def _totalChangeSell(self):
        self.totalChangeSell = self.addChangeSell - self.deleteChangeSell

    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    currency_id = fields.Many2one('res.currency', related="company_id.currency_id", string="Currency", readonly=True)

    supplier_po_id = fields.Many2one('project.supplier_po', string='Supplier PO', readonly=True)

    cnNumber = fields.Char(string = 'Change Notice', required = True) # TODO: Autonumerado
    creationDate = fields.Date(string='Date')
    description = fields.Char(string='Description', required=True)

    deleteAmount =fields.Monetary(string='Delete', currency_field='currency_id', required = False)
    deleteMargin = fields.Float(string='Delete Margin (%)', required = False)
    deleteChangeSell = fields.Monetary(compute="_deleteChangeSell", string="Delete Change Sell") # TODO: Review the formula

    addAmount =fields.Monetary(string='Add', currency_field='currency_id', required = False)
    addMargin = fields.Float(string='Add Margin (%)', required = False)
    addChangeSell = fields.Monetary(compute="_addChangeSell", string="Add Change Sell") # TODO: Review the formula

    totalChangeCost = fields.Monetary(compute="_totalChangeCost", string="Total Change Cost")
    totalChangeSell = fields.Monetary(compute="_totalChangeSell", string="Total Change Sell")

    #revisedSell = ? #TODO: Review this formula

    @api.multi
    def show_va_changeNotices(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'project.change_notices',  # this model
            'res_id': self.id,  # the current wizard record
            'view_mode': 'form',
            'view_type': 'form',
            'views': [[False, 'form']],
            'context': {'form_view_initial_mode': 'edit'},
        }

    
