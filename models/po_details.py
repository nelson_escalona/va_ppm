    # -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date
# from odoo.addons import decimal_precision as dp

class VApodetails(models.Model):
    _name = "project.po_details"
    # _description = 'Purchase Order Line'
    # _inherit = ['project.project']
    _inherit = ['mail.thread']

    @api.one
    @api.depends('product_id')
    def _computedName(self):
        self.name = self.product_id.display_name


    # Todo:
    # @api.one
    # @api.onchange('product_id')
    # def product_id_change(self):
    #     self.description = self.product_id.description

    @api.multi
    @api.onchange('comments')
    def comment_change(self):
        self.commentsDate = date.today()

    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 default=lambda self: self.env.user.company_id)
    currency_id = fields.Many2one('res.currency', related="company_id.currency_id", string="Currency",
                                  readonly=True)

    supplier_po_id = fields.Many2one('project.supplier_po', string='Supplier PO', readonly=True)

    product_id = fields.Many2one('product.product', string='Product', change_default=True, required=True)

    name = fields.Text(string="Name", compute="_computedName")

    description = fields.Text(string='Description')
    product_uom_qty = fields.Float(string='Ordered Quantity', default=1.0)
    product_uom = fields.Many2one('uom.uom', string='Unit of Measure')

    confirmUnitDeliveryToSite = fields.Text(string='Confirm Unit Delivey to Site')
    deliveryDate = fields.Date(string='Delivery Lead Date')

    dateReleasedMfg = fields.Date(string='Released MFG Date')
    dateEstimatedShip = fields.Date(string='Estimated Ship Date')

    qtyTotalShipped = fields.Float(string='Total Qty Shipped', default=1.0)
    qtyRemainingToBeShipped = fields.Float(string='Qty Remaining to be Shipped', default=1.0)
    dateReceived = fields.Date(string='Estimated Ship Date')
    qtyReceived = fields.Float(string='Total Qty Shipped', default=1.0)
    dateDamagedOrWarranty = fields.Date(string='Damage or Warranty Reported ')
    dateDamagedOrWarrantyMFG = fields.Date(string='Damage or Warranty Reported to MFG')
    dateInvoice = fields.Date(string='Full or Partial Invoice (Date)')
    dateEstimatedCorrectionShip = fields.Date(string='Date Estimated Correction Ship')
    isDamaged = fields.Boolean(string='Any Damage?')
    dateRemainingInvoice = fields.Date(string='Date Remaining Invoice')
    comments = fields.Text(string='Comments')
    commentsDate = fields.Date(string='Comments Date', readonly=True)

    @api.multi
    def show_po_details(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'project.po_details',  # this model
            'res_id': self.id,  # the current wizard record
            'view_mode': 'form',
            'view_type': 'form',
            'views': [[False, 'form']],
            'context': {'form_view_initial_mode': 'edit'},
        }