# -*- coding: utf-8 -*-

from odoo import models, fields, api
from . import utils

class VASupplierPO(models.Model):
    _name = 'project.supplier_po'
    _inherit = ['mail.thread']

    @api.depends('baseSellPrice', 'baseCost')
    @api.one
    def _computeMargin(self):
        self.baseMargin = utils.margin(self.baseSellPrice, self.baseCost)

    @api.depends('invoice_id')
    @api.multi
    def _computeBilled2Date(self):
        for po in self:
            self.billedToDate = sum(invoice.billingSell for invoice in po.invoice_id)

    @api.depends('invoice_id')
    @api.multi
    def _computeCost2Date(self):
        for po in self:
            self.costToDate = sum(invoice.billingCost for invoice in po.invoice_id)

    @api.depends('currentSellPrice', 'billedToDate')
    @api.one
    def _computeRemaininToBeBilled(self):
        self.remainingToBeBilled = self.currentSellPrice - self.billedToDate

    @api.depends('currentCost', 'costToDate')
    @api.one
    def _remainingCost(self):
        self.remainingCost = self.currentCost - self.costToDate

    @api.depends('baseSellPrice', 'change_notices_id')
    @api.multi
    def _computeCurrentSellPrice(self):
        for po in self:
            self.currentSellPrice = self.baseSellPrice + sum(cn.totalChangeSell for cn in po.change_notices_id)

    @api.depends('baseCost', 'change_notices_id')
    @api.multi
    def _computeCurrentCost(self):
        for po in self:
            self.currentCost = self.baseCost + sum(cn.totalChangeCost for cn in po.change_notices_id)

    @api.depends('currentSellPrice', 'currentCost')
    @api.one
    def _computeCurrentMargin(self):
        self.currentMargin = utils.margin(self.currentSellPrice, self.currentCost)


    project_id = fields.Many2one('project.project', 'Project')
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    currency_id = fields.Many2one('res.currency', related="company_id.currency_id", string="Currency", readonly=True)

    supplier_id = fields.Many2one('res.partner', string='Supplier/Manufacturer', auto_join=True, track_visibility='onchange', required=True)

    drawings_id = fields.One2many('project.drawings', 'supplier_po_id') #string='Change Notices', auto_join=True, track_visibility='onchange')

    name = fields.Char(string='PO Number', required=True, track_visibility='always', index=True)

    baseSellPrice = fields.Monetary(string='Base PO Sell Price', currency_field='currency_id', required=True)
    baseCost = fields.Monetary(string='Base PO Cost', currency_field='currency_id', required=True)
    baseMargin = fields.Float(compute="_computeMargin", string="Base Margin", store=True)

    billedToDate = fields.Monetary(compute="_computeBilled2Date", string="Billed to Date", currency_field='currency_id', store=True)
    costToDate = fields.Monetary(compute="_computeCost2Date", string="Cost to Date", currency_field='currency_id', store=True)

    remainingToBeBilled = fields.Monetary(compute="_computeRemaininToBeBilled", string="Remaining to be Billed", currency_field='currency_id', store=True)
    remainingCost = fields.Monetary(compute="_remainingCost", string="Cost Remaining",  currency_field='currency_id', store=True)

    currentSellPrice = fields.Monetary(compute="_computeCurrentSellPrice", string="Current Sell Price",  currency_field='currency_id',store=True)
    currentCost = fields.Monetary(compute="_computeCurrentCost", string="Current Cost",  currency_field='currency_id', store=True)
    currentMargin = fields.Float(compute="_computeCurrentMargin", string="Current Margin", store=True)

    change_notices_id = fields.One2many('project.change_notices', 'supplier_po_id', 'Change Notice', help='The list of Change Notice')
    invoice_id = fields.One2many('project.invoice', 'supplier_po_id', 'Invoice', help='The list of Invoice')

    po_details_id = fields.One2many('project.po_details', 'supplier_po_id', 'PO Details', help='The list of Items')

    # if poStatus = true then is a PO else costToCome
    isPO = fields.Boolean(string="PO / CTC", help="True is a PO, False is Cost to come", default=False)

    @api.multi
    def show_va_changeNotices(self):
        self.ensure_one()
        view_id = self.env.ref('va_ppm.supplierpo_form').id#added
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'project.supplier_po',  # this model
            'res_id': self.id,  # the current wizard record
            'view_mode': 'form',
            # 'view_type': 'form',
            #'views': [[False, 'form']],
            'view_id': view_id, #added
            'context': {'form_view_initial_mode': 'edit'},
        }

    
