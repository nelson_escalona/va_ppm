

def margin(sell, cost):
    try:
        result = (1 - cost / sell) * 100.0
    except:
        result = 0

    return result
